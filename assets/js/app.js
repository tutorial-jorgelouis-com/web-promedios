const slctCurso = document.querySelector('#select-curso');
const slctNota = document.querySelector('#select-nota');
const btnLimpiar = document.querySelector('#button-limpiar');
const btnAgregar = document.querySelector('#button-agregar');
const lstCursos = document.querySelector('#lista-cursos');
const spanPromedio = document.querySelector('#span-promedio');

let arrayNotas = [];
let promedio = 0;
spanPromedio.textContent = promedio;

btnLimpiar.addEventListener('click', limpiar);
btnAgregar.addEventListener('click', agregar);

function limpiar(){
  //console.log('limpiar'+slctCurso.value);
  slctCurso.value = '';
  slctNota.value = '';
}

function agregar(){
  const curso = slctCurso.value;
  const nota = slctNota.value;
  
  //Primero verificamos que los campos no estén vacíos
  if(curso.trim().length > 0 && nota.trim().length > 0){
    //Verificamos que la nota se encuentre en el rango permitido (0 - 7)
    if(nota >= 0 && nota <= 7){
      const html = `
        <ion-item>
          ${curso}: ${nota}
        </ion-item>
      `;

      lstCursos.insertAdjacentHTML('afterbegin', html);
      limpiar();

      /////////////////////////////////////////////////
      //CÁLCULO DEL PROMEDIO SIMPLE
      
      //Nos aseguramos que la nota sea un número y no texto
      //Agregamos la nueva nota al array de todas las notas ingresadas
      arrayNotas.push(Number(nota));

      //Calculamos la suma de las notas ingresadas
      let suma = 0;

      for(let posicion in arrayNotas){
        suma = suma + arrayNotas[posicion];
      }

      //El promedio será la suma de notas / número de notas ingresadas
      promedio = suma / arrayNotas.length;

      //Mostramos el valor
      spanPromedio.textContent = Math.round(promedio);
      /////////////////////////////////////////////////
    }else{
      alert('La nota está fuera del rango permitido (0 - 7).');
    }
  }else{
    //console.log('Valores no válidos');
    //Esto genera un pop up con el mensaje
    alert('Por favor complete el formulario.');
  }
}